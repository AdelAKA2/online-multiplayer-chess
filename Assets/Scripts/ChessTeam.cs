using UnityEngine;

public enum ChessTeam
{
    None,
    White,
    Black
}