using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Security.Cryptography;

public class ChessUI : MonoBehaviour
{
    public static ChessUI Instance { get; private set; }

    [SerializeField] TMP_Text currentPlayerText;

    private void Awake()
    {
        Instance = this;
    }

    public void UpdatePlayerText()
    {
        if(GameController.Instance.currentPlayer == ChessTeam.White)
        {
            currentPlayerText.text = "White";
        }
        else
        {
            currentPlayerText.text = "Black";
        }
    }

    public void SurrenderButton()
    {
        ChessTeam loser = (GameController.Instance.GetLocalPlayer() == ChessTeam.White) ? ChessTeam.Black : ChessTeam.White;
        Board.Instance.EndGameAndSetWinner(loser);
    }
}
