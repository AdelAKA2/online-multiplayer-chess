using ExitGames.Client.Photon.StructWrapping;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController Instance { get; private set; } 

    [SerializeField] Transform cameraWhiteTransform;
    [SerializeField] Transform cameraBlackTransform;

    public ChessTeam currentPlayer;
    private ChessTeam localPlayer;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        currentPlayer = ChessTeam.White;
    }

    public void ResetVariables()
    {
        currentPlayer = ChessTeam.White;
    }

    public bool CheckIfCurrentPlayer()
    {
        return currentPlayer == localPlayer;
    }

    public void ChangePlayer()
    {
        currentPlayer = (currentPlayer == ChessTeam.White) ? ChessTeam.Black : ChessTeam.White;
        Board.Instance.GenerateOccupationMatrix();
        ChessUI.Instance.UpdatePlayerText();
        if(SinglePlayerBoard.Instance != null && currentPlayer != localPlayer)
        {
            SinglePlayerBoard.Instance.AiMakeMove(currentPlayer);
        }
    }

    public void SetupCamera(ChessTeam team)
    {
        if(team == ChessTeam.White) Camera.main.transform.SetPositionAndRotation(cameraWhiteTransform.position, cameraWhiteTransform.rotation);
        else Camera.main.transform.SetPositionAndRotation(cameraBlackTransform.position, cameraBlackTransform.rotation);
    }

    public void SetLocalPlayer(ChessTeam team)
    {
        localPlayer = team;
        Debug.Log(localPlayer);
    }

    public ChessTeam GetLocalPlayer() { return localPlayer; }
}
