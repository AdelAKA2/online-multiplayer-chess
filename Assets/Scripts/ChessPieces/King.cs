using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class King : ChessPiece
{
    private readonly List<Vector2Int> KING_TRANSITIONS = new List<Vector2Int>
        {
            new Vector2Int(1, 0),
            new Vector2Int(1, 1),
            new Vector2Int(0, 1),
            new Vector2Int(-1, 1),
            new Vector2Int(-1, 0),
            new Vector2Int(-1, -1),
            new Vector2Int(0, -1),
            new Vector2Int(1, -1),
        };

    public override void GenerateTransitions(ChessTeam[,] occupationMatrix)
    {
        PossibleTransitions.Clear();

        Vector2Int appliedTransition;
        foreach (Vector2Int transition in KING_TRANSITIONS)
        {
            appliedTransition = ApplyAddition(transition);
            if (isDebug)
                Debug.Log(appliedTransition);
            if (Board.Instance.CoorInBoardRange(appliedTransition)
                && occupationMatrix[appliedTransition.x, appliedTransition.y] != pieceType)
                PossibleTransitions.Add(appliedTransition);
        }
    }

    public override void UpdateAdditionalInfo()
    {
    }
}
