using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ChessPiece : MonoBehaviour
{
    public Vector2Int initialCoordinates;
    public Vector2Int CurrentCoordinates { get; set; }
    public List<Vector2Int> PossibleTransitions { get; set; }

    public ChessTeam pieceType;
    public ChessTeam OppositType
    {
        get
        {
            if (pieceType == ChessTeam.Black) return ChessTeam.White;
            else return ChessTeam.Black;
        }
    }

    [SerializeField] protected bool isDebug = false;

    protected virtual void Awake()
    {
        CurrentCoordinates = initialCoordinates;
        PossibleTransitions = new List<Vector2Int>();
    }

    public abstract void GenerateTransitions(ChessTeam[,] occupationMatrix);
    public abstract void UpdateAdditionalInfo();

    protected Vector2Int ApplyAddition(Vector2Int addedValue)
    {
        if (pieceType == ChessTeam.White) addedValue *= -1;
        return CurrentCoordinates + addedValue;
    }
}
