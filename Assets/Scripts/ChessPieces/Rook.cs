using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rook : ChessPiece
{
    public override void GenerateTransitions(ChessTeam[,] occupationMatrix)
    {
        PossibleTransitions.Clear();

        Vector2Int appliedTransition;
        // Check FrontLine
        for (int i = 1; i < Board.Instance.CHESS_BOARD_DIMENTIONS; i++)
        {
            appliedTransition = ApplyAddition(new Vector2Int(0, i));
            if (!CheckCondition(occupationMatrix, appliedTransition)) break;
        }
        // Check BackLine
        for (int i = 1; i < Board.Instance.CHESS_BOARD_DIMENTIONS; i++)
        {
            appliedTransition = ApplyAddition(new Vector2Int(0, -i));
            if (!CheckCondition(occupationMatrix, appliedTransition)) break;
        }
        // Check RightLine
        for (int i = 1; i < Board.Instance.CHESS_BOARD_DIMENTIONS; i++)
        {
            appliedTransition = ApplyAddition(new Vector2Int(i, 0));
            if (!CheckCondition(occupationMatrix, appliedTransition)) break;
        }
        // Check LeftLine
        for (int i = 1; i < Board.Instance.CHESS_BOARD_DIMENTIONS; i++)
        {
            appliedTransition = ApplyAddition(new Vector2Int(-i, 0));
            if (!CheckCondition(occupationMatrix, appliedTransition)) break;
        }
    }

    private bool CheckCondition(ChessTeam[,] occupationMatrix, Vector2Int _appliedTransition)
    {
        if (!Board.Instance.CoorInBoardRange(_appliedTransition)
            || occupationMatrix[_appliedTransition.x, _appliedTransition.y] == pieceType)
            return false;
        if (occupationMatrix[_appliedTransition.x, _appliedTransition.y] == OppositType)
        {
            PossibleTransitions.Add(_appliedTransition);
            return false;
        }
        PossibleTransitions.Add(_appliedTransition);
        return true;
    }

    public override void UpdateAdditionalInfo()
    {
    }
}
