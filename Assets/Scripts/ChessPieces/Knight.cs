using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knight : ChessPiece
{
    private readonly List<Vector2Int> KNIGHT_TRANSITIONS = new List<Vector2Int>
        {
            // forward
            new Vector2Int(1, 2),
            new Vector2Int(-1, 2),

            // backward
            new Vector2Int(1, -2),
            new Vector2Int(-1, -2),

            // right
            new Vector2Int(2, 1),
            new Vector2Int(2, -1),

            // left
            new Vector2Int(-2, 1),
            new Vector2Int(-2, -1),
        };

    public override void GenerateTransitions(ChessTeam[,] occupationMatrix)
    {
        PossibleTransitions.Clear();

        Vector2Int appliedTransition;
        foreach (Vector2Int transition in KNIGHT_TRANSITIONS)
        {
            appliedTransition = ApplyAddition(transition);
            if (isDebug)
                Debug.Log(appliedTransition);
            if (Board.Instance.CoorInBoardRange(appliedTransition)
                && occupationMatrix[appliedTransition.x, appliedTransition.y] != pieceType)
                PossibleTransitions.Add(appliedTransition);
        }
    }

    public override void UpdateAdditionalInfo()
    {
    }
}
