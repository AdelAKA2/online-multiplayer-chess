using ExitGames.Client.Photon.StructWrapping;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;

public class Pawn : ChessPiece
{
    private bool isfirstMove;
    private readonly List<Vector2Int> FORWARD_TRANSITIONS = new List<Vector2Int>
        {
            new Vector2Int(0, 1),
            new Vector2Int(0, 2)
        };
    private readonly Vector2Int FRONT_RIGHT_CORNER= new Vector2Int(1, 1);
    private readonly Vector2Int FRONT_LEFT_CORNER = new Vector2Int(-1, 1);

    protected override void Awake()
    {
        base.Awake();
        isfirstMove = true;
    }

    public override void GenerateTransitions(ChessTeam[,] occupationMatrix)
    {
        PossibleTransitions.Clear();

        Vector2Int appliedTransition;
        for (int i = 0; i < FORWARD_TRANSITIONS.Count; i++)
        {
            appliedTransition = ApplyAddition(FORWARD_TRANSITIONS[i]);

            if (!Board.Instance.CoorInBoardRange(appliedTransition)
                || occupationMatrix[appliedTransition.x, appliedTransition.y] != ChessTeam.None) break;
            PossibleTransitions.Add(appliedTransition);
            if (!isfirstMove) break;
        }

        // Check Front Right Corner
        appliedTransition = ApplyAddition(FRONT_RIGHT_CORNER);
        if (Board.Instance.CoorInBoardRange(appliedTransition)
            && occupationMatrix[appliedTransition.x, appliedTransition.y] == OppositType)
            PossibleTransitions.Add(appliedTransition);

        // Check Front Left Corner
        appliedTransition = ApplyAddition(FRONT_LEFT_CORNER);
        if (Board.Instance.CoorInBoardRange(appliedTransition)
            && occupationMatrix[appliedTransition.x, appliedTransition.y] == OppositType)
            PossibleTransitions.Add(appliedTransition);

        if (isDebug) PossibleTransitions.ForEach(x => Debug.Log(x));

    }

    public override void UpdateAdditionalInfo()
    {
        isfirstMove = false;
    }
}

