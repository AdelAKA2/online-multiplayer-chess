using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    private const string TEAM = "team";
    [SerializeField] StartScreenChessUI startScreenChessUI;

    private void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    private void Update()
    {
        if (startScreenChessUI != null)
            startScreenChessUI.SetConnectionStatusText(PhotonNetwork.NetworkClientState.ToString());
    }

    public void Connect()
    {
        if (PhotonNetwork.IsConnected) startScreenChessUI.ShowConnectingPanel();
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        Debug.LogError($"Welcome Player {PhotonNetwork.LocalPlayer.ActorNumber}");
        startScreenChessUI.ShowConnectingPanel();
    }

    public void CreateRoom()
    {
        PhotonNetwork.CreateRoom(startScreenChessUI.rooomNameInputText.text, new RoomOptions
        {
            MaxPlayers = 2
        });
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.LogError($"Room {startScreenChessUI.rooomNameInputText.text} Already Exist");
    }

    public void JoinRoom()
    {
        PhotonNetwork.JoinRoom(startScreenChessUI.rooomNameInputText.text);
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.Log("No such rooom");
    }

    public override void OnJoinedRoom()
    {
        Debug.LogError($"Player {PhotonNetwork.LocalPlayer.ActorNumber} joined room");
        PhotonNetwork.LocalPlayer.SetCustomProperties(new ExitGames.Client.Photon.Hashtable() { { TEAM, -1 } });

        PrepareTeamSelectionOptions();
        startScreenChessUI.ShowTeamSelectionPanel();
    }

    private void PrepareTeamSelectionOptions()
    {
        if (PhotonNetwork.CurrentRoom.PlayerCount > 1)
        {
            var player = PhotonNetwork.CurrentRoom.GetPlayer(1);
            if (player.CustomProperties.ContainsKey(TEAM))
            {
                var occupiedTeam = player.CustomProperties[TEAM];
                if((int)occupiedTeam != -1)
                    startScreenChessUI.RestrictTeamChoice((ChessTeam)occupiedTeam);
            }
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.LogError($"Player {newPlayer.ActorNumber} entered the room");
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        MultiPlayerBoard.Instance.EndGameAndSetWinner(GameController.Instance.GetLocalPlayer());
    }

    public void SetTeam(ChessTeam team)
    {
        if (PhotonNetwork.IsConnected)
        {
            if (PhotonNetwork.CurrentRoom.PlayerCount > 1)
            {
                var player = PhotonNetwork.CurrentRoom.GetPlayer(1);
                if (player.CustomProperties.ContainsKey(TEAM))
                {
                    var occupiedTeam = player.CustomProperties[TEAM];
                    team = (ChessTeam)occupiedTeam == ChessTeam.White ? ChessTeam.Black : ChessTeam.White;
                }
            }
            PhotonNetwork.LocalPlayer.SetCustomProperties(new ExitGames.Client.Photon.Hashtable() { { TEAM, team } });
        }
    }

    public void EndGame()
    {
        if (PhotonNetwork.InRoom)
            PhotonNetwork.LeaveRoom();
    }
}
