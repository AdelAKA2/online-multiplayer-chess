using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class Board : MonoBehaviour
{
    //public static Board Instance { get; private set; }

    [SerializeField] protected List<Node> nodes;
    [SerializeField] protected List<ChessPiece> blackChessPieces;
    [SerializeField] protected List<ChessPiece> whiteChessPieces;
    [SerializeField] Vector2 blockDimentions;
    [SerializeField] Transform startTransform;
    [SerializeField] Material whiteMat;
    [SerializeField] Material blackMat;

    private ChessTeam[,] currentOccupationMatrix;

    public Node SelectedNode;

    public int CHESS_BOARD_DIMENTIONS = 8;
    public int CHESS_BOARD_SIZE = 64;

    public static Board Instance
    {
        get
        {
            if (MultiPlayerBoard.Instance != null) return MultiPlayerBoard.Instance;
            else return SinglePlayerBoard.Instance;
        }
    }

    private void Awake()
    {
        //Instance = this;
    }

    private void Start()
    {
        SelectedNode = null;
        currentOccupationMatrix = new ChessTeam[CHESS_BOARD_DIMENTIONS, CHESS_BOARD_DIMENTIONS];

        CHESS_BOARD_SIZE = CHESS_BOARD_DIMENTIONS * CHESS_BOARD_DIMENTIONS;

        PrepareNodes();
        PreparePieces();
        GenerateOccupationMatrix();
    }

    private void PrepareNodes()
    {
        Vector3 newPosition = startTransform.position;
        Material newMat = blackMat;

        for (int j = 0; j < CHESS_BOARD_DIMENTIONS; j++)
        {
            for (int i = 0; i < CHESS_BOARD_DIMENTIONS; i++)
            {
                nodes[CoorToVec(i, j)].transform.position = newPosition;
                nodes[CoorToVec(i, j)].GetComponent<Renderer>().material = newMat;
                nodes[CoorToVec(i, j)].Coordinates = new Vector2Int(i, j);

                newMat = (newMat == whiteMat) ? blackMat : whiteMat;
                newPosition.x += blockDimentions.x;
            }
            newPosition.x = startTransform.position.x;
            newPosition.z -= blockDimentions.y;
            newMat = (newMat == whiteMat) ? blackMat : whiteMat;
        }
    }

    public void PreparePieces()
    {
        for (int i = 0; i < blackChessPieces.Count; i++)
        {
            nodes[CoorToVec(blackChessPieces[i].initialCoordinates.x, blackChessPieces[i].initialCoordinates.y)].PositionChessPiece(blackChessPieces[i]);
            nodes[CoorToVec(whiteChessPieces[i].initialCoordinates.x, whiteChessPieces[i].initialCoordinates.y)].PositionChessPiece(whiteChessPieces[i]);
        }
    }

    public void ShowHideAvailableMoves()
    {
        foreach (Vector2Int possibleMove in SelectedNode.OccupiedPiece.PossibleTransitions)
        {
            nodes[CoorToVec(possibleMove.x, possibleMove.y)].HighlightDehighlight(SelectedNode.OccupiedPiece.pieceType);
        }
    }

    public void CheckMove(Vector2Int newNodeCoor)
    {
        if (nodes[CoorToVec(newNodeCoor.x, newNodeCoor.y)].OccupiedPiece == null)
        {
            if (!SelectedNode.OccupiedPiece.PossibleTransitions.Any(coor => coor == newNodeCoor)) return;
            ShowHideAvailableMoves();
            MakeMove(newNodeCoor);
        }

        else if (nodes[CoorToVec(newNodeCoor.x, newNodeCoor.y)].OccupiedPiece.pieceType == SelectedNode.OccupiedPiece.pieceType)
        {
            ShowHideAvailableMoves();
            UnselectNode();
        }

        else if (nodes[CoorToVec(newNodeCoor.x, newNodeCoor.y)].OccupiedPiece.pieceType != SelectedNode.OccupiedPiece.pieceType)
        {
            if (!SelectedNode.OccupiedPiece.PossibleTransitions.Any(coor => coor == newNodeCoor)) return;
            ShowHideAvailableMoves();
            EatPiece(newNodeCoor);
            CheckGameStat();
        }
    }

    private void CheckGameStat()
    {
        if (whiteChessPieces.Count == 0)
        {
            EndGameAndSetWinner(ChessTeam.Black);
        }
        else if (blackChessPieces.Count == 0)
        {
            EndGameAndSetWinner(ChessTeam.White);
        }
    }

    public void GenerateOccupationMatrix()
    {
        foreach (Node node in nodes)
        {
            currentOccupationMatrix[node.Coordinates.x, node.Coordinates.y] = node.OccupiedPieceType;
        }
        InvokePiecesToGeneratePossibleMoves();
    }

    private void InvokePiecesToGeneratePossibleMoves()
    {
        if (GameController.Instance.currentPlayer == ChessTeam.Black)
        {
            foreach (ChessPiece blackChessPiece in blackChessPieces)
            {
                blackChessPiece.GenerateTransitions(currentOccupationMatrix);
            }
        }
        else
        {
            foreach (ChessPiece whiteChessPiece in whiteChessPieces)
            {
                whiteChessPiece.GenerateTransitions(currentOccupationMatrix);
            }
        }
    }

    protected abstract void MakeMove(Vector2Int newNodeCoor);
    protected abstract void EatPiece(Vector2Int newNodeCoor);
    public abstract void SelectNode(Vector2Int nodeCoor);
    protected abstract void UnselectNode();
    public abstract void EndGameAndSetWinner(ChessTeam winner);

    protected int CoorToVec(int x, int y)
    {
        return x * CHESS_BOARD_DIMENTIONS + y;
    }

    protected Vector2 VecToCoor(int index)
    {
        return new Vector2(index % CHESS_BOARD_SIZE, index / CHESS_BOARD_SIZE);
    }

    public bool CoorInBoardRange(Vector2Int coorToCheck)
    {
        if (coorToCheck.x > -1 && coorToCheck.x < CHESS_BOARD_DIMENTIONS && coorToCheck.y > -1 && coorToCheck.y < CHESS_BOARD_DIMENTIONS) return true;
        return false;
    }
}
