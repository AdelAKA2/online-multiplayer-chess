using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInitializer : MonoBehaviour
{
    public static GameInitializer Instance { get; private set; }

    [SerializeField] Transform boardInstantiationTransform;
    [SerializeField] SinglePlayerBoard singlePlayerBoardPrefab;
    [SerializeField] MultiPlayerBoard multiPlayerBoardPrefab;
    [SerializeField] NetworkManager networkManager;
    [SerializeField] StartScreenChessUI startScreenChessUI;
    [SerializeField] GameObject gamePanel;

    private GameMode gameMode;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        gamePanel.SetActive(false);
    }

    public void InitializeGame(GameMode _gameMode, int teamIndex)
    {
        ChessTeam team = (teamIndex == 1) ? ChessTeam.White : ChessTeam.Black;
        GameController.Instance.SetLocalPlayer(team);
        GameController.Instance.SetupCamera(team);
        gamePanel.SetActive(true);
        GameController.Instance.ResetVariables();

        gameMode = _gameMode;
        if (gameMode == GameMode.SinglePlayer) InitializeSinglePlayer(team);
        else InitializeMultiPlayer(team);
    }

    public void InitializeSinglePlayer(ChessTeam team)
    {
        Instantiate(singlePlayerBoardPrefab, boardInstantiationTransform.position, boardInstantiationTransform.rotation);
    }

    public void InitializeMultiPlayer(ChessTeam team)
    {
        networkManager.SetTeam(team);
        if(Board.Instance == null)
            PhotonNetwork.Instantiate(multiPlayerBoardPrefab.name, boardInstantiationTransform.position, boardInstantiationTransform.rotation).GetComponent<MultiPlayerBoard>();
    }

    public void EndGame(ChessTeam winner)
    {
        startScreenChessUI.ShowEndGamePanel(winner);
        gamePanel.SetActive(false);
    }
}
