using ExitGames.Client.Photon.StructWrapping;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class MultiPlayerBoard : Board
{
    private PhotonView photonView;

    public static new MultiPlayerBoard Instance;

    private void Awake()
    {
        Instance = this;
        photonView = GetComponent<PhotonView>();
    }

    protected override void MakeMove(Vector2Int newNodeCoor)
    {
        photonView.RPC(nameof(RPC_PositionChessPiece), RpcTarget.AllBuffered, new object[] { CoorToVec(newNodeCoor.x, newNodeCoor.y) });
        UnselectNode();
    }

    [PunRPC]
    private void RPC_PositionChessPiece(int newNodeIndex)
    {
        nodes[newNodeIndex].PositionChessPiece(SelectedNode.OccupiedPiece);
        SelectedNode.RemoveOccupiedPiece();

        GameController.Instance.ChangePlayer();
    }

    protected override void EatPiece(Vector2Int newNodeCoor)
    {
        photonView.RPC(nameof(RPC_EatChessPiece), RpcTarget.AllBuffered, new object[] { CoorToVec(newNodeCoor.x, newNodeCoor.y) });
        UnselectNode();
    }

    [PunRPC]
    private void RPC_EatChessPiece(int newNodeIndex)
    {
        if (nodes[newNodeIndex].OccupiedPiece.pieceType == ChessTeam.White) whiteChessPieces.Remove(nodes[newNodeIndex].OccupiedPiece);
        else blackChessPieces.Remove(nodes[newNodeIndex].OccupiedPiece);

        nodes[newNodeIndex].OccupiedPiece.gameObject.SetActive(false);
        if (nodes[newNodeIndex].OccupiedPiece.IsType<King>())
        {
            ChessTeam winner = (nodes[newNodeIndex].OccupiedPiece.pieceType == ChessTeam.White) ? ChessTeam.Black : ChessTeam.White;
            Board.Instance.EndGameAndSetWinner(winner);
        }

        nodes[newNodeIndex].PositionChessPiece(SelectedNode.OccupiedPiece);
        SelectedNode.RemoveOccupiedPiece();

        GameController.Instance.ChangePlayer();
    }

    public override void SelectNode(Vector2Int nodeCoor)
    {
        photonView.RPC(nameof(RPC_SelectNode), RpcTarget.AllBuffered, new object[] { CoorToVec(nodeCoor.x, nodeCoor.y) });
    }

    [PunRPC]
    private void RPC_SelectNode(int nodeIndex)
    {
        SelectedNode = nodes[nodeIndex];
    }

    protected override void UnselectNode()
    {
        SelectedNode.SelectDeselect();
        photonView.RPC(nameof(RPC_UnselectNode), RpcTarget.AllBuffered);
    }

    [PunRPC]
    private void RPC_UnselectNode()
    {
        SelectedNode = null;
    }

    public override void EndGameAndSetWinner(ChessTeam winner)
    {
        photonView.RPC(nameof(RPC_EndGameAndSetWinner), RpcTarget.AllBuffered, new object[] { winner });
    }

    [PunRPC]
    private void RPC_EndGameAndSetWinner(ChessTeam winner)
    {
        GameInitializer.Instance.EndGame(winner);
        Destroy(gameObject);
    }
}
