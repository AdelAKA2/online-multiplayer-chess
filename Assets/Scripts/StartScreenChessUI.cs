using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum GameMode
{
    SinglePlayer,
    MultiPlayer
}

public class StartScreenChessUI : MonoBehaviour
{

    [SerializeField] NetworkManager networkManager;

    [Header("Game Panels")]
    [SerializeField] GameObject startPanel;
    [SerializeField] GameObject connectPanel;
    [SerializeField] GameObject teamSelectPanel;
    [SerializeField] GameObject endGamePanel;

    [Header("Buttons")]
    [SerializeField] Button whiteTeamButton;
    [SerializeField] Button blackTeamButton;

    [Header("Connection UI")]
    public TMP_Text rooomNameInputText;
    [SerializeField] TMP_Text winnerText;

    [SerializeField] TMP_Text connectionStatus;

    private GameMode gameMode = GameMode.SinglePlayer;

    private void Awake()
    {
        OnGameLaunched();
    }

    private void OnGameLaunched()
    {
        ShowStartPanel();
    }

    public void OnSinglePlayerModeSelcted()
    {
        gameMode = GameMode.SinglePlayer;
        ShowTeamSelectionPanel();
    }

    public void OnMultiPlayerModeSelcted()
    {
        gameMode = GameMode.MultiPlayer;
        networkManager.Connect();
    }

    public void ShowStartPanel()
    {
        DisableAllPanels();
        startPanel.SetActive(true);
    }

    public void SetConnectionStatusText(string status)
    {
        connectionStatus.text = status;
    }

    public void ShowConnectingPanel()
    {
        DisableAllPanels();
        connectPanel.SetActive(true);
    }

    public void ShowTeamSelectionPanel()
    {
        DisableAllPanels();
        teamSelectPanel.SetActive(true);
    }

    public void ShowEndGamePanel(ChessTeam winner)
    {
        DisableAllPanels();
        endGamePanel.SetActive(true);
        if (winner == ChessTeam.White)
        {
            winnerText.text = "White Won";
            winnerText.color = Color.white;
        }
        else
        {
            winnerText.text = "Black Won";
            winnerText.color = Color.black;
        }
    }

    private void DisableAllPanels()
    {
        startPanel.SetActive(false);
        connectPanel.SetActive(false);
        teamSelectPanel.SetActive(false);
        endGamePanel.SetActive(false);
    }

    public void ReturnToMenuButton()
    {
        ShowStartPanel();
        whiteTeamButton.interactable = true;
        blackTeamButton.interactable = true;
        networkManager.EndGame();
    }

    public void SelectTeamButton(int teamIndex)
    {
        GameInitializer.Instance.InitializeGame(gameMode, teamIndex);
        DisableAllPanels();
    }

    public void RestrictTeamChoice(ChessTeam occupiedTeam)
    {
        var buttonToDeactivate = occupiedTeam == ChessTeam.White ? whiteTeamButton : blackTeamButton;
        buttonToDeactivate.interactable = false;
    }
}
