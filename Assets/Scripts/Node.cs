using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Node : MonoBehaviour, IPointerDownHandler
{
    [SerializeField] GameObject highlightedAreaSelect;
    [SerializeField] GameObject highlightedAreaEat;

    public float offset;

    public Vector2Int Coordinates { get; set; }
    public ChessPiece OccupiedPiece { get; set; }
    public ChessTeam OccupiedPieceType { get; set; }

    private void Awake()
    {
        OccupiedPiece = null;
        OccupiedPieceType = ChessTeam.None;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!GameController.Instance.CheckIfCurrentPlayer()) return;
        if (Board.Instance.SelectedNode == null)
        {
            if (OccupiedPiece != null && GameController.Instance.currentPlayer == OccupiedPiece.pieceType)
            {
                Board.Instance.SelectNode(Coordinates);
                SelectDeselect();
                Board.Instance.ShowHideAvailableMoves();
            }
        }
        else
        {
            Board.Instance.CheckMove(Coordinates);
        }
    }

    public void PositionChessPiece(ChessPiece chessPiece)
    {
        Vector3 newPos = transform.position;
        newPos.y += offset;
        chessPiece.transform.position = newPos;
        chessPiece.CurrentCoordinates = Coordinates;
        OccupiedPiece = chessPiece;
        OccupiedPieceType = chessPiece.pieceType;
    }

    public void RemoveOccupiedPiece()
    {
        OccupiedPiece.UpdateAdditionalInfo();
        OccupiedPiece = null;
        OccupiedPieceType = ChessTeam.None;
    }

    public void SelectDeselect()
    {
        highlightedAreaSelect.SetActive(!highlightedAreaSelect.activeSelf);
    }

    public void HighlightDehighlight(ChessTeam selectedPieceType)
    {
        if (OccupiedPieceType == ChessTeam.None)
            highlightedAreaSelect.SetActive(!highlightedAreaSelect.activeSelf);
        else if (OccupiedPieceType != selectedPieceType)
            highlightedAreaEat.SetActive(!highlightedAreaEat.activeSelf);
    }
}
