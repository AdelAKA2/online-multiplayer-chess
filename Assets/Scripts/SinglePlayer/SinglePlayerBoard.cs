using ExitGames.Client.Photon.StructWrapping;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class SinglePlayerBoard : Board
{
    public static new SinglePlayerBoard Instance;

    private void Awake()
    {
        Instance = this;
    }

    protected override void MakeMove(Vector2Int newNodeCoor)
    {
        nodes[CoorToVec(newNodeCoor.x, newNodeCoor.y)].PositionChessPiece(SelectedNode.OccupiedPiece);
        SelectedNode.RemoveOccupiedPiece();
        UnselectNode();

        GameController.Instance.ChangePlayer();
    }

    protected override void EatPiece(Vector2Int newNodeCoor)
    {
        int newNodeIndex = CoorToVec(newNodeCoor.x, newNodeCoor.y);

        if (nodes[newNodeIndex].OccupiedPiece.pieceType == ChessTeam.White) whiteChessPieces.Remove(nodes[newNodeIndex].OccupiedPiece);
        else blackChessPieces.Remove(nodes[newNodeIndex].OccupiedPiece);

        nodes[newNodeIndex].OccupiedPiece.gameObject.SetActive(false);
        if (nodes[newNodeIndex].OccupiedPiece.IsType<King>())
        {
            ChessTeam winner = (nodes[newNodeIndex].OccupiedPiece.pieceType == ChessTeam.White) ? ChessTeam.Black : ChessTeam.White;
            Board.Instance.EndGameAndSetWinner(winner);
        }

        nodes[newNodeIndex].PositionChessPiece(SelectedNode.OccupiedPiece);
        SelectedNode.RemoveOccupiedPiece();
        UnselectNode();

        GameController.Instance.ChangePlayer();
    }

    public override void SelectNode(Vector2Int nodeCoor)
    {
        SelectedNode = nodes[CoorToVec(nodeCoor.x, nodeCoor.y)];
    }

    protected override void UnselectNode()
    {
        SelectedNode.SelectDeselect();
        SelectedNode = null;
    }

    public override void EndGameAndSetWinner(ChessTeam winner)
    {
        GameInitializer.Instance.EndGame(winner);
        Destroy(gameObject);
    }

    public void AiMakeMove(ChessTeam aiTeam)
    {
        bool chosePiece = false;
        ChessPiece chosenChessPiece = null;
        int choiceCount = 0;
        do
        {
            choiceCount++;
            if (choiceCount == 20)
            {
                Debug.Log("No possible move");
                break;
            }
            if (aiTeam == ChessTeam.Black)
            {
                chosenChessPiece = blackChessPieces[Random.Range(0, blackChessPieces.Count)];
                if (chosenChessPiece.PossibleTransitions.Count > 0) chosePiece = true;
            }
            else
            {
                chosenChessPiece = whiteChessPieces[Random.Range(0, whiteChessPieces.Count)];
                if (chosenChessPiece.PossibleTransitions.Count > 0) chosePiece = true;
            }
        } while (!chosePiece);

        if (choiceCount < 20)
        {
            SelectedNode = nodes[CoorToVec(chosenChessPiece.CurrentCoordinates.x, chosenChessPiece.CurrentCoordinates.y)];
            SelectedNode.SelectDeselect();
            Board.Instance.ShowHideAvailableMoves();
            CheckMove(chosenChessPiece.PossibleTransitions[Random.Range(0, chosenChessPiece.PossibleTransitions.Count)]);

        }
        else
        {
            GameController.Instance.ChangePlayer();
        }
    }
}
